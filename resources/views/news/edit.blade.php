@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Redaguoti naujiena: {{ $newsItem->title }}</h1>
            </div>

            <div class="col-sm-8">
                <form method="POST" action="{{ route('news.update', $newsItem->id) }}">
                    @csrf
                    <div class="form-group">
                        <input type="text" value="{{ $newsItem->title }}" name="title" class="form-control" />
                    </div>

                    <div class="form-group">
                        <textarea class="form-control" name="content">{{ $newsItem->content }}</textarea>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Redaguoti" />
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection