@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Sukurti nauja naujiena</h1>
            </div>

            <div class="col-sm-8">
                <form method="POST" action="{{ route('news.store') }}">
                    @csrf
                    <div class="form-group">
                        <input type="text" name="title" class="form-control" />
                    </div>

                    <div class="form-group">
                        <textarea class="form-control" name="content">
                        </textarea>
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Issaugot" />
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection