    @extends('layouts.app')

    @section('content')
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1>Naujienos</h1>
                </div>
                @foreach($news as $newsItem)
                    <div class="col-sm-12">
                        <h3>
                            <a href="{{ route('news.show', $newsItem->id) }}">
                                {{ $newsItem->title }}
                            </a>
                        </h3>
                    </div>
                @endforeach
            </div>
        </div>
    @endsection