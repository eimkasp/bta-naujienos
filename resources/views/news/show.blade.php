@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <h1>{{ $newsItem->title }}</h1>
                <p>
                    {{ $newsItem->content }}
                </p>
            </div>

            <div class="col-sm-4">
                <form method="post" action="{{ route('news.destroy', $newsItem->id) }}">
                    @csrf
                    <input type="submit" value="Trinti" class="btn btn-danger" />
                </form>

                <a href="{{ route('news.edit', $newsItem->id) }}" class="btn btn-warning">
                    Redaguoti
                </a>
            </div>


        </div>
    </div>
@endsection