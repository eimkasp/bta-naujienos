<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    //

	public function index(Request $request) {
		$news = News::all();

		return view('news.index', compact(['news']));
	}

	public function show($id) {
		$newsItem = News::findOrFail($id);

		return view('news.show', compact('newsItem'));
	}


	// Sukurimo formos atvaizdavimas
	public function create() {
		return view('news.create');
	}


	public function store(Request $request) {

		// sukuriame news modelio objekta
		$newsItem = new News();
		$newsItem->title = $request->input('title');
		$newsItem->content = $request->input('content');
		$newsItem->author_id = 1;

		// isssaugome naujiena duombazeje
		$newsItem->save();

		return redirect()->route('news.show', $newsItem->id);
		//return redirect()->route('news.index');
//		return redirect()->back();
	}

	public function destroy($id) {
		$newsItem = News::findOrFail($id);

		// jei iskviesiu delete funkcija, pasirinkta naujiena, bus istrinta
		$newsItem->delete();

		return redirect()->route('news.index');
	}

	public function edit($id) {

		$newsItem = News::find($id);

		return view('news.edit', compact('newsItem'));
	}



	public function update(Request $request, $id) {

		// gauname is duombazes naujienos objekta
		$newsItem = News::findOrFail($id);

		// irasome naujas reiksmes
		$newsItem->title = $request->input('title');
		$newsItem->content = $request->input('content');
		// issaugome duombazeje
		$newsItem->save();

		return redirect()->route('news.show', $newsItem->id);
	}

}
